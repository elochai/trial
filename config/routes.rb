Rails.application.routes.draw do
  mount ActionCable.server, at: "/cable"
  devise_for :users
  resources :tasks, only: [:create, :update, :destroy]
  root to: 'home#index'
end
