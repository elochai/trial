import React from 'react';
import apiClient from '../../../utils/apiClient'
import { Modal, Button, FormGroup, FormControl, ControlLabel } from 'react-bootstrap';

export default class AddTaskModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      show: this.props.show,
      validationError: null,
      newTask: { description: '', performer_id: this.props.currentUserId }
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleDescriptionChange = this.handleDescriptionChange.bind(this);
    this.handlePerformerChange = this.handlePerformerChange.bind(this);
    this.handleShowModal = this.handleShowModal.bind(this);
    this.handleCloseModal = this.handleCloseModal.bind(this);
  };

  handleShowModal() {
    this.setState({ show: true });
  };

  handleCloseModal() {
    this.setState({ show: false });
    this.props.handleCloseModal();
  };

  handleDescriptionChange(event) {
    if (event.target.value.length) {
      this.setState({
        ...this.state,
        validationError: null,
        newTask: {...this.state.newTask, description: event.target.value}
      });
    } else {
      this.setState({
        ...this.state,
        validationError: 'this field is required',
        newTask: {...this.state.newTask, description: event.target.value}
      });
    }
  };

  handlePerformerChange(event) {
    this.setState({...this.state, newTask: {...this.state.newTask, performer_id: event.target.value}});
  };

  handleSubmit(event) {
    event.preventDefault();
    if (!this.state.newTask.description.length) {
      this.setState({...this.state, validationError: 'this field is required'});
    } else {
      apiClient.post(`/tasks`, this.state.newTask)
        .then(res => {
          this.props.prependTaskToParent(res.data);
          this.setState({
            ...this.state,
            validationError: null,
            newTask: {description: '', performer_id: this.props.currentUserId}
          });
          this.handleCloseModal()
        })
    }
  };

  render() {
    return (
      <div>
        <Modal show={this.state.show} onHide={this.handleCloseModal}>
          <Modal.Header>
            <Modal.Title>Add a new task</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <form>
              <FormGroup controlId="formControlsTextarea" validationState={this.state.validationError ? 'error' : null}>
                <ControlLabel>Task description</ControlLabel>
                <FormControl
                  componentClass="textarea"
                  placeholder="My new task"
                  value={this.state.newTask.description}
                  onChange={this.handleDescriptionChange}
                />
                {this.state.validationError && <ControlLabel>{this.state.validationError}</ControlLabel>}
              </FormGroup>

              <FormGroup controlId="formControlsSelect">
                <ControlLabel>Select performer</ControlLabel>
                <FormControl
                  componentClass="select"
                  placeholder="Select"
                  onChange={this.handlePerformerChange}
                  value={this.state.newTask.performer_id}
                >
                  {
                    this.props.users.map(user => {
                      return (
                        <option key={user.id} value={user.id}>
                          {user.email}
                        </option>
                      );
                    })
                  }
                </FormControl>
              </FormGroup>
            </form>
          </Modal.Body>
          <Modal.Footer>
            <Button type="submit" bsStyle="primary" onClick={this.handleSubmit}>Create</Button>
            <Button onClick={this.handleCloseModal}>Close</Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}
