import React from 'react';
import apiClient from '../../../utils/apiClient';
import AddTaskModal from './AddTaskModal';
import EditTaskModal from './EditTaskModal';
import {Table, Label, Button, ButtonGroup} from 'react-bootstrap';
import actionCable from 'actioncable';
import Alert from 'react-s-alert';

import 'react-s-alert/dist/s-alert-default.css';
import 'react-s-alert/dist/s-alert-css-effects/slide.css';

const alertOptions = { position: 'bottom-right', effect: 'slide', timeout: 5000};

export default class TaskList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {tasks: this.props.tasks, editTask: {}, showAddModal: false, showEditModal: false};
    this.prependTask = this.prependTask.bind(this);
    this.updateTask = this.updateTask.bind(this);
    this.removeTask = this.removeTask.bind(this);
    this.handleShowAddModal = this.handleShowAddModal.bind(this);
    this.handleCloseAddModal = this.handleCloseAddModal.bind(this);
    this.handleCloseEditModal = this.handleCloseEditModal.bind(this);
    this.updateTasksFromChannel = this.updateTasksFromChannel.bind(this);
    this.addBlink = this.addBlink.bind(this);
    this.removeBlink = this.removeBlink.bind(this);
  };

  componentDidMount() {
    const CableApp = {};
    const cableUrl = process.env.NODE_ENV === 'development' ? `ws://${window.location.hostname}:3000/cable` : `wss://${window.location.hostname}/cable`;
    CableApp.cable = actionCable.createConsumer(cableUrl);
    CableApp.user = CableApp.cable.subscriptions.create({channel: "UserTasksChannel", user_id: this.props.currentUser.id}, {
      received: (payload) => {
        this.updateTasksFromChannel(payload);
      }
    })
  }

  updateTasksFromChannel(payload) {
    if (payload.action === 'destroy') {
      this.removeTask(payload.task.id, payload.message);
    } else if (payload.action === 'create') {
      this.prependTask(payload.task, payload.message);
    } else if (payload.action === 'update') {
      this.updateTask(payload.task, payload.message);
    }
  };

  addBlink() {
    if (this.state.blinkTaskId) {
      const blinkElement = document.getElementById(`task-${this.state.blinkTaskId}`);
      blinkElement.setAttribute('class', 'Blink');
    }
  }

  removeBlink() {
    if (this.state.blinkTaskId) {
      const blinkElement = document.getElementById(`task-${this.state.blinkTaskId}`);
      blinkElement.setAttribute('class', 'Task');
    }
  }

  handleShowAddModal() {
    this.setState({showAddModal: true})
  };

  handleCloseAddModal() {
    this.setState({showAddModal: false})
  };

  handleCloseEditModal() {
    this.setState({showEditModal: false})
  };

  prependTask(task, alertMsg = 'New task added') {
    this.setState({...this.state, blinkTaskId: task.id, tasks: [task, ...this.state.tasks] });
    Alert.success(alertMsg, alertOptions);
    this.addBlink();
  };

  updateTask(updatedTask, alertMsg = 'Task updated') {
    this.setState({
      ...this.state,
      blinkTaskId: updatedTask.id,
      tasks: this.state.tasks.map(task =>
        (task.id === updatedTask.id)
          ? updatedTask
          : task
      )
    });
    Alert.info(alertMsg, alertOptions);
    this.addBlink();
    setTimeout(this.removeBlink, 1000);
  };

  removeTask(task_id, alertMsg = 'Task deleted') {
    this.setState({
      ...this.state,
      tasks: this.state.tasks.filter(task =>
        task.id !== task_id
      )
    });
    Alert.error(alertMsg, alertOptions)
  };

  handleEditTask = (task) => {
    this.setState({...this.state, editTask: task, showEditModal: true})
  };

  deleteTask = (task_id) => {
    apiClient.delete(`/tasks/${task_id}`)
      .then(() => {
        this.removeTask(task_id);
      })
  };

  compareTaskValues(key, order='asc') {
    return function(a, b) {
      const varA = a[key].toUpperCase();
      const varB = b[key].toUpperCase();
      let comparison = 0;
      if (varA > varB) {
        comparison = 1;
      } else if (varA < varB) {
        comparison = -1;
      }
      return (
        (order === 'desc') ? (comparison * -1) : comparison
      );
    };
  }

  sortTasks = (event, sortKey) => {
    const sortOrder = event.target.getAttribute('data-sort-order') || 'asc';
    const tasks = this.state.tasks;

    tasks.sort(this.compareTaskValues(sortKey, sortOrder));
    this.setState({tasks});

    const nextSortOrder = sortOrder === 'asc' ? 'desc' : 'asc';
    const thArray = document.getElementsByTagName('th');
    for(let i = 0; i < thArray.length; i++) {
      thArray[i].setAttribute('class', '')
    }

    event.target.setAttribute('class', nextSortOrder);
    event.target.setAttribute('data-sort-order', nextSortOrder)
  };

  render() {
    return (
      <div className="TaskList">
        <div className="TaskListHeading">
          {this.state.tasks.length > 0 ? <h3>My tasks</h3> : <h3>No tasks :(</h3>}
          <span>
            <Button bsStyle="success" onClick={this.handleShowAddModal}>Add a new one!</Button>
          </span>
        </div>
        {this.state.tasks.length > 0 &&
          <Table responsive>
            <thead className="thead-dark">
            <tr>
              <th onClick={e => this.sortTasks(e, 'description')}>Description</th>
              <th onClick={e => this.sortTasks(e, 'state')}>State</th>
              <th onClick={e => this.sortTasks(e, 'owner_email')}>Owner</th>
              <th onClick={e => this.sortTasks(e, 'performer_email')}>Performer</th>
              <th data-sort-order="asc" className="asc" colSpan="2" onClick={e => this.sortTasks(e, 'created_at')}>Created</th>
            </tr>
            </thead>
            <tbody>
            {
              this.state.tasks.map(task => {
                return (
                    <tr id={`task-${task.id}`} key={task.id}>
                      <td><strong>{task.description}</strong></td>
                      <td><Label bsStyle={task.state === 'open' ? 'default' : 'success'}>{task.state}</Label></td>
                      <td>{task.owner_email}</td>
                      <td>{task.performer_email}</td>
                      <td>{task.created_at}</td>
                      <td>
                        <ButtonGroup bsSize="small">
                          <Button bsStyle="primary" onClick={() => this.handleEditTask(task)}>Edit</Button>
                          {task.owner_id === this.props.currentUser.id &&
                            <Button bsStyle="danger" onClick={() => this.deleteTask(task.id)}>Delete</Button>
                          }
                        </ButtonGroup>
                      </td>
                    </tr>
                );
              })
            }
            </tbody>
          </Table>
        }
        {this.state.showAddModal && <AddTaskModal
          show={this.state.showAddModal}
          currentUserId={this.props.currentUser.id}
          users={this.props.users}
          handleCloseModal={this.handleCloseAddModal}
          prependTaskToParent={this.prependTask}
        />}
        {this.state.showEditModal && this.state.tasks.length > 0 && <EditTaskModal
          show={this.state.showEditModal}
          currentUserId={this.props.currentUser.id}
          users={this.props.users}
          task={this.state.editTask}
          handleCloseModal={this.handleCloseEditModal}
          rerenderTaskInParent={this.updateTask}
        />}
        <Alert stack={{limit: 5}} />
      </div>
    );
  }
}
