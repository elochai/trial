class TasksController < ApplicationController
  before_action :authenticate_user!
  before_action :set_task, only: [:update, :destroy]
  before_action :init_broadcaster, only: [:create, :update, :destroy]

  rescue_from ActiveRecord::RecordNotFound do |e|
    render json: { message: e.message}, status: :not_found
  end

  rescue_from ActiveRecord::RecordInvalid do |e|
    render json: { message: e.message }, status: :unprocessable_entity
  end

  def create
    @task = current_user.owned_tasks.create!(task_params)
    @task_broadcaster.task = @task
    @task_broadcaster.broadcast
    render json: @task, status: :created
  end

  def update
    @task_broadcaster.broadcast if @task.update(task_params)
    render json: @task, status: :ok
  end

  def destroy
    return head :unauthorized if @task.owner != current_user
    @task_broadcaster.broadcast if @task.destroy
    head :no_content
  end

  private

  def task_params
    params_allowed = [:description, :state]
    params_allowed << :performer_id if action_name == 'create' || @task.owner == current_user
    params.permit(params_allowed)
  end

  def set_task
    @task = current_user.tasks.find(params[:id])
  end

  def init_broadcaster
    @task_broadcaster =
      TaskBroadcastService.new(
        task: @task,
        performer_was: @task&.performer,
        action: action_name,
        current_user: current_user)
  end
end
