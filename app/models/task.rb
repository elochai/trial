# frozen_string_literal: true

class Task < ApplicationRecord
  STATES = { open: 0, done: 1 }

  belongs_to :owner, class_name: 'User'
  belongs_to :performer, class_name: 'User'

  validates :owner, :performer, :description, :state, presence: true

  enum state: STATES

  default_scope { order(created_at: :desc) }
end
