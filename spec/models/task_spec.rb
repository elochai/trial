require "rails_helper"

describe Task, type: :model do
  it { expect(subject).to belong_to(:owner).class_name('User') }
  it { expect(subject).to belong_to(:performer).class_name('User') }
  it { is_expected.to validate_presence_of(:owner) }
  it { is_expected.to validate_presence_of(:performer) }
  it { is_expected.to validate_presence_of(:description) }
  it { is_expected.to validate_presence_of(:state) }
end
