import React from 'react';
import apiClient from '../../../utils/apiClient'
import { Modal, Button, FormGroup, FormControl, ControlLabel } from 'react-bootstrap';

export default class EditTaskModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {show: this.props.show, task: this.props.task, validationError: null};
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleDescriptionChange = this.handleDescriptionChange.bind(this);
    this.handlePerformerChange = this.handlePerformerChange.bind(this);
    this.handleStateChange = this.handleStateChange.bind(this);
    this.handleShowModal = this.handleShowModal.bind(this);
    this.handleCloseModal = this.handleCloseModal.bind(this);
  };

  handleShowModal() {
    this.setState({ show: true });
  };

  handleCloseModal() {
    this.setState({ show: false });
    this.props.handleCloseModal();
  };

  handleDescriptionChange(event) {
    if (event.target.value.length) {
      this.setState({
        ...this.state,
        validationError: null,
        task: {...this.state.task, description: event.target.value}
      });
    } else {
      this.setState({
        ...this.state,
        validationError: 'this field is required',
        task: {...this.state.task, description: event.target.value}
      });
    }
  };

  handlePerformerChange(event) {
    this.setState({...this.state, task: {...this.state.task, performer_id: event.target.value}});
  };

  handleStateChange(event) {
    this.setState({...this.state, task: {...this.state.task, state: event.target.value}});
  };

  handleSubmit(event) {
    event.preventDefault();
    if (!this.state.task.description.length) {
      this.setState({...this.state, validationError: 'this field is required'});
    } else {
      apiClient.put(`/tasks/${this.state.task.id}`, this.state.task)
        .then(res => {
          this.props.rerenderTaskInParent(res.data);
          this.setState({...this.state, validationError: null, task: {}});
          this.handleCloseModal()
        })
    }
  };

  render() {
    return (
        <div>
          <Modal show={this.state.show} onHide={this.handleCloseModal}>
            <Modal.Header>
              <Modal.Title>Edit task</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <form>
                <FormGroup controlId="formControlsTextarea" validationState={this.state.validationError ? 'error' : null}>
                  <ControlLabel>Task description</ControlLabel>
                  <FormControl
                      componentClass="textarea"
                      placeholder="My task"
                      value={this.state.task.description}
                      onChange={this.handleDescriptionChange}
                  />
                  {this.state.validationError && <ControlLabel>{this.state.validationError}</ControlLabel>}
                </FormGroup>

                {this.state.task.owner_id === this.props.currentUserId &&
                  <FormGroup controlId="formControlsSelect">
                    <ControlLabel>Select performer</ControlLabel>
                    <FormControl
                        componentClass="select"
                        placeholder="Select"
                        onChange={this.handlePerformerChange}
                        value={this.state.task.performer_id}
                    >
                      {
                        this.props.users.map(user => {
                          return (
                            <option key={user.id} value={user.id}>
                              {user.email}
                            </option>
                          );
                        })
                      }
                    </FormControl>
                  </FormGroup>
                }

                <FormGroup controlId="formControlsSelectMultiple">
                  <ControlLabel>Select state</ControlLabel>
                  <FormControl
                    componentClass="select"
                    placeholder="Select"
                    onChange={this.handleStateChange}
                    value={this.state.task.state}
                  >
                    <option value="open">Open</option>
                    <option value="done">Done</option>
                  </FormControl>
                </FormGroup>

                <FormGroup>
                  <ControlLabel>Creation date</ControlLabel>
                  <FormControl.Static>{this.state.task.created_at}</FormControl.Static>
                </FormGroup>
              </form>
            </Modal.Body>
            <Modal.Footer>
              <Button type="submit" bsStyle="primary" onClick={this.handleSubmit}>Update</Button>
              <Button onClick={this.handleCloseModal}>Close</Button>
            </Modal.Footer>
          </Modal>
        </div>
    );
  }
}
