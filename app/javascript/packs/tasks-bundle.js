import ReactOnRails from 'react-on-rails';

import TaskList from '../bundles/Tasks/components/TaskList';
import '../bundles/Tasks/components/TaskList.css';
import '../bootstrap.css';
import { setCSRFToken } from '../utils/apiClient'

setCSRFToken();
ReactOnRails.register({
  TaskList,
});
