class CreateTasks < ActiveRecord::Migration[5.2]
  def change
    create_table :tasks do |t|
      t.belongs_to :owner, index: true
      t.belongs_to :performer, index: true
      t.integer :state, null: false, default: 0
      t.text :description, null: false, default: ''
      t.datetime :created_at, null: false
    end
  end
end
