class UserTasksChannel < ApplicationCable::Channel
  def subscribed
    @user = User.find_by(id: params[:user_id])
    stream_for @user
  end

  def received(data)
    UserTasksChannel.broadcast_to(@user, {tasks: data})
  end
end
