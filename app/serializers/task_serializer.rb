class TaskSerializer < ActiveModel::Serializer
  attributes :id, :description, :state, :owner_id, :owner_email, :performer_id, :performer_email, :created_at

  def owner_email
    object.owner.email
  end

  def performer_email
    object.performer.email
  end

  def created_at
    object.created_at.strftime("%b %d, %Y @ %H:%M:%S UTC")
  end
end
