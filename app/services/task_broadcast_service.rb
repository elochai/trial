class TaskBroadcastService
  attr_writer :task

  def initialize(task:, performer_was:, action:, current_user:)
    @task = task
    @performer_was = performer_was
    @action = action
    @current_user = current_user
  end

  def broadcast
    receiver = channel_receiver
    return unless receiver

    UserTasksChannel.broadcast_to(receiver, channel_payload)
  end

  private

  def channel_receiver
    return @performer_was if @task.performer == @task.owner && @performer_was != @current_user
    receiver = @task.performer == @current_user ? @task.owner : @task.performer
    receiver if receiver != @current_user
  end

  def channel_payload
    payload = { task: TaskSerializer.new(@task).serializable_hash }

    if task_was_assigned?
      message = "#{@current_user.email} assigned you on task '#{@task.description}'"
      action = 'create'
    elsif task_performer_reassigned?
      message = "#{@current_user.email} removed you from task '#{@task.description}'"
      action = 'destroy'
    elsif task_was_deleted?
      message = "#{@current_user.email} deleted task '#{@task.description}'"
      action = @action
    else
      message = "#{@current_user.email} updated task '#{@task.description}'"
      action = @action
    end

    payload[:message] = message
    payload[:action] = action
    payload
  end

  def task_was_assigned?
    (@performer_was == @current_user &&
      @task.performer != @performer_was &&
      @action == 'update') ||
      @action == 'create'
  end

  def task_performer_reassigned?
    @performer_was != @current_user &&
      @task.performer != @performer_was &&
      @action == 'update'
  end

  def task_was_deleted?
    @action == 'destroy'
  end
end
