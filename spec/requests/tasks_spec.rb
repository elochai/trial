require 'rails_helper'

describe 'Tasks API', type: :request do
  let!(:user1) { create(:user) }
  let!(:user2) { create(:user) }
  let!(:task1) { create(:task, owner: user1, performer: user2) }
  let!(:task2) { create(:task, owner: user2, performer: user1) }
  let!(:task_id) { task1.id }

  before { sign_in(user1) }

  describe 'POST /tasks' do
    context 'when the request is valid' do
      before { post '/tasks', params: { description: 'Test Description', performer_id: user2.id } }

      it 'creates a task' do
        expect(JSON.parse(response.body)['description']).to eq('Test Description')
      end

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
    end

    context 'when the request is invalid' do
      before { post '/tasks', params: { description: nil, performer_id: user2.id } }

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns a validation failure message' do
        expect(response.body).to match(/Validation failed/)
      end
    end
  end

  describe 'PUT /tasks/:id' do
    context 'when the record exists' do
      before { put "/tasks/#{task_id}", params: { description: 'Updated desc' } }

      it 'updates the record' do
        expect(JSON.parse(response.body)).not_to be_empty
        expect(JSON.parse(response.body)['id']).to eq(task_id)
        expect(JSON.parse(response.body)['description']).to eq('Updated desc')
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when the request is invalid' do
      before { post '/tasks', params: { description: nil } }

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns a validation failure message' do
        expect(response.body).to match(/Validation failed/)
      end
    end

    context 'when current user is not a task owner' do
      before { put "/tasks/#{task2.id}", params: { performer_id: user2.id } }

      it 'does not update performer_id' do
        expect(JSON.parse(response.body)['performer_id']).not_to eq(user2.id)
        expect(JSON.parse(response.body)['performer_id']).to eq(user1.id)
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end
  end

  describe 'DELETE /tasks/:id' do
    context 'when current user is a task owner' do
      before { delete "/tasks/#{task_id}" }

      it 'returns status code 204' do
        expect(response).to have_http_status(204)
      end
    end

    context 'when current user is not a task owner' do
      before { delete "/tasks/#{task2.id}" }

      it 'returns status code 401' do
        expect(response).to have_http_status(401)
      end
    end
  end
end
