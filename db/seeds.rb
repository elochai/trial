puts "********Seeding Data Start************"

user1 = User.where(email: 'steffen@email.com').first_or_create(password: '123123123')
user2 = User.where(email: 'bohdan@email.com').first_or_create(password: '123123123')
Task.create(description: "Steffen's task", owner: user1, performer: user1)
Task.create(description: "Bohdan's task", owner: user2, performer: user2)
Task.create(description: "Steffen's task for Bohdan", owner: user1, performer: user2)
Task.create(description: "Bohdan's task for Steffen", owner: user2, performer: user1)

puts "********Seeding Data End************"
