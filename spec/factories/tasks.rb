FactoryBot.define do
  factory :task do
    sequence(:description) { |n| "test task ##{n}" }
    state 0
    owner { create(:user) }
    performer { create(:user) }
  end
end
