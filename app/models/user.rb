class User < ApplicationRecord
  devise :database_authenticatable, :rememberable, :trackable, :validatable

  has_many :owned_tasks, class_name: "Task", foreign_key: "owner_id"
  has_many :assigned_tasks, class_name: "Task", foreign_key: "performer_id"

  def tasks
    Task.where(owner: self).or(Task.where(performer: self))
  end
end
