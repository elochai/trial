import axios from 'axios';

export default axios.create({
  baseURL: `${window.location.protocol}//${window.location.host}`
});

export function setCSRFToken() {
  const csrfToken = document.querySelector("meta[name=csrf-token]").content;
  if (csrfToken) {
    axios.defaults.headers.common['X-CSRF-Token'] = csrfToken;
  }
}
