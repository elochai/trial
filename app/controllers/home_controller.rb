# frozen_string_literal: true

class HomeController < ApplicationController
  layout "react"
  before_action :authenticate_user!

  def index
    @props = formatted_props
  end

  private

  def formatted_props
    hash = { currentUser: current_user, users: User.all.select(:id, :email) }
    hash[:tasks] = ActiveModel::SerializableResource.new(current_user.tasks).serializable_hash
    hash
  end
end
