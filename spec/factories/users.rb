FactoryBot.define do
  factory :user do
    sequence(:email) { |n| "test_user#{n}@email.com" }
    password 'password'
  end
end
